<# Author: Paul Robson

Modified By: Sam Lewis

Description: This scripts is set to run the first time a new VM starts up.
		First it reads credentials used to join VM to domain. Then it activates VM with license key.
		It will also install certificates needed for DSC encryption
		You can set a static IP address from here, or leave it uncommented for DHCP. DNS servers can
			also be set here if those cmdlets are uncommented.
#>

$Data = Import-csv -Path "c:\programdata\scripts\prod-adCreds.csv"
$realdomain = $Data.DomainName
$domcred = $Data.dompass
$Cred2 = ConvertTo-SecureString -String $domcred -Force -AsPlainText
$Admincreds = New-Object System.Management.Automation.PSCredential ("$(($realdomain -split '\.')[0])\$(($Data.Domainadmin))", $Cred2)

$localpc = $env:computername

## Activate Windows via PS script
## Hyper-V guests are activated via their host using publicly-available AVMA keys - see https://discourse.clcohio.org/t/1470/24
$key = "TMJ3Y-NTRTM-FJYXT-T22BY-CWG3J"
$service = get-wmiObject -query "select * from SoftwareLicensingService" -computername $localpc
$service.InstallProductKey($key)
$service.RefreshLicenseStatus()

## Creates password for private certificates, and installs certificates in VM
## Certificates here are used for DSC encryption

#$mypwd = ConvertTo-SecureString -String "XXXXXX" -Force -AsPlainText
#Import-PfxCertificate -FilePath "C:\powershell-dsc\publickey\DSCCert.pfx" -CertStoreLocation Cert:\LocalMachine\My -Password $mypwd
#Import-PfxCertificate -FilePath "C:\powershell-dsc\publickey\DSCPwdCertPFX.pfx" -CertStoreLocation Cert:\LocalMachine\my -Password $mypwd

## UNCOMMENT IF will use static IP address for VM
Get-NetAdapter | New-NetIPAddress -IPAddress "192.168.44.75" -PrefixLength 24 -DefaultGateway "192.168.44.1"
## UNCOMMENT IF will use static IP address / DNS for VM
Get-NetAdapter | Set-DnsClientServerAddress -ServerAddresses "192.168.44.15","192.168.44.16"

## UNCOMMENT to not have IPv6 interfere at time of joining VM to domain
Get-NetAdapter | Where-Object { $_.Name -like "Ethernet*" } | Disable-NetAdapterBinding -ComponentID ms_tcpip6

## Let script / cmdlets work
Start-Sleep -seconds 60

## Sets SecondStart.cmd file to run at startup for next time VM starts
NEW-ITEMPROPERTY "HKLM:\Software\Microsoft\Windows\CurrentVersion\RunOnce\" -Name "SecondStart" -Value "C:\ProgramData\Scripts\SecondStart.cmd"

## Add local computer to a domain using credentials and restarting
Add-Computer -ComputerName $localpc -DomainName $realdomain -Credential $Admincreds -Restart