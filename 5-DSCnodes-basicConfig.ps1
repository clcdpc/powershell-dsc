<# Author: Jason Helmick and Jeffrey Snover
#
# Modified by: Paul Robson, Sam Lewis
#
# Description: This script/module for DSC node has some basic Windows features to install.
# 	It also copies all necessary DSC resource modules from a share in DSC server to the DSC node
# 	
#
#> 

## This is where DSC nodes get declared and specific roles added to them
## Any node should use the same Cert file, thumbpring and PSDscAllowDomainUser
## NodeName = name of computer that will be DSC node
$configData = @{
    AllNodes = @(

		@{
            NodeName = 'DC10-15'
            Role = @('DHCP','ADDS','DC10-15','All')
            CertificateFile = "C:\powershell-dsc\publickey\DSCPwdCert.cer"
            Thumbprint = "DDB37AEF86B21E298B3CD817E658F9CEFA1088C6"
            PSDscAllowDomainUser = $true
        }
		
        @{
            NodeName = 'DC144-15'
            Role = @('ADDS','DC144-15','All')
            CertificateFile = "C:\powershell-dsc\publickey\DSCPwdCert.cer"
            Thumbprint = "DDB37AEF86B21E298B3CD817E658F9CEFA1088C6"
            PSDscAllowDomainUser = $true
        }

        @{
            NodeName = 'DC144-16'
            Role = @('ADDS','DC144-16','All')
            CertificateFile = "C:\powershell-dsc\publickey\DSCPwdCert.cer"
            Thumbprint = "DDB37AEF86B21E298B3CD817E658F9CEFA1088C6"
            PSDscAllowDomainUser = $true
        }
		
        @{
            NodeName = 'DC44-15'
            Role = @('ADDS','DC44-15','All')
            CertificateFile = "C:\powershell-dsc\publickey\DSCPwdCert.cer"
            Thumbprint = "DDB37AEF86B21E298B3CD817E658F9CEFA1088C6"
            PSDscAllowDomainUser = $true
        }

        @{
            NodeName = 'DC44-16'
            Role = @('ADDS','DC44-16','All')
            CertificateFile = "C:\powershell-dsc\publickey\DSCPwdCert.cer"
            Thumbprint = "DDB37AEF86B21E298B3CD817E658F9CEFA1088C6"
            PSDscAllowDomainUser = $true
        }

        @{
            NodeName = 'DC45-15'
            Role = @('ADDS','DC45-15','All')
            CertificateFile = "C:\powershell-dsc\publickey\DSCPwdCert.cer"
            Thumbprint = "DDB37AEF86B21E298B3CD817E658F9CEFA1088C6"
            PSDscAllowDomainUser = $true
        }

        @{
            NodeName = 'DC45-16'
            Role = @('ADDS','DC45-16','All')
            CertificateFile = "C:\powershell-dsc\publickey\DSCPwdCert.cer"
            Thumbprint = "DDB37AEF86B21E298B3CD817E658F9CEFA1088C6"
            PSDscAllowDomainUser = $true
        }
		
		@{
            NodeName = 'PRODDFS16'
            Role = @('DFSS','All')
            CertificateFile = "C:\powershell-dsc\publickey\DSCPwdCert.cer"
            Thumbprint = "DDB37AEF86B21E298B3CD817E658F9CEFA1088C6"
            PSDscAllowDomainUser = $true
        }
		
		@{
            NodeName = 'PRODBACKUP01'
            Role = @('BACKUP','All')
            CertificateFile = "C:\powershell-dsc\publickey\DSCPwdCert.cer"
            Thumbprint = "DDB37AEF86B21E298B3CD817E658F9CEFA1088C6"
            PSDscAllowDomainUser = $true
        }
		
		@{
            NodeName = 'DevWeb'
            Role = @('WEB','All')
            CertificateFile = "C:\powershell-dsc\publickey\DSCPwdCert.cer"
            Thumbprint = "DDB37AEF86B21E298B3CD817E658F9CEFA1088C6"
            PSDscAllowDomainUser = $true
        }

		@{
            NodeName = 'ProdWeb16'
            Role = @('WEB','All')
            CertificateFile = "C:\powershell-dsc\publickey\DSCPwdCert.cer"
            Thumbprint = "DDB37AEF86B21E298B3CD817E658F9CEFA1088C6"
            PSDscAllowDomainUser = $true
        }

        @{
            NodeName = 'DevApp'
            Role = @('APP','All')
            CertificateFile = "C:\powershell-dsc\publickey\DSCPwdCert.cer"
            Thumbprint = "DDB37AEF86B21E298B3CD817E658F9CEFA1088C6"
            PSDscAllowDomainUser = $true
        }

        @{
            NodeName = 'ProdApp16'
            Role = @('APP','All')
            CertificateFile = "C:\powershell-dsc\publickey\DSCPwdCert.cer"
            Thumbprint = "DDB37AEF86B21E298B3CD817E658F9CEFA1088C6"
            PSDscAllowDomainUser = $true
        }

        @{
            NodeName = 'ProdDb16'
            Role = @('APP','All')
            CertificateFile = "C:\powershell-dsc\publickey\DSCPwdCert.cer"
            Thumbprint = "DDB37AEF86B21E298B3CD817E658F9CEFA1088C6"
            PSDscAllowDomainUser = $true
        }

        @{
            NodeName = 'DevPac'
            Role = @('PAC','HTTPSredirect','All')
            CertificateFile = "C:\powershell-dsc\publickey\DSCPwdCert.cer"
            Thumbprint = "DDB37AEF86B21E298B3CD817E658F9CEFA1088C6"
            PSDscAllowDomainUser = $true
        }

        @{
            NodeName = 'ProdPac16'
            Role = @('PAC','HTTPSredirect','All')
            CertificateFile = "C:\powershell-dsc\publickey\DSCPwdCert.cer"
            Thumbprint = "DDB37AEF86B21E298B3CD817E658F9CEFA1088C6"
            PSDscAllowDomainUser = $true
        }

        @{
            NodeName = 'DevSR'
            Role = @('SR','HTTPSredirect','All')
            CertificateFile = "C:\powershell-dsc\publickey\DSCPwdCert.cer"
            Thumbprint = "DDB37AEF86B21E298B3CD817E658F9CEFA1088C6"
            PSDscAllowDomainUser = $true
        }
        
        @{
            NodeName = 'ProdSR16'
            Role = @('SR','HTTPSredirect','All')
            CertificateFile = "C:\powershell-dsc\publickey\DSCPwdCert.cer"
            Thumbprint = "DDB37AEF86B21E298B3CD817E658F9CEFA1088C6"
            PSDscAllowDomainUser = $true
        }

        @{
            NodeName = 'ProdPAPI'
            Role = @('HTTPSredirect','All')
            CertificateFile = "C:\powershell-dsc\publickey\DSCPwdCert.cer"
            Thumbprint = "DDB37AEF86B21E298B3CD817E658F9CEFA1088C6"
            PSDscAllowDomainUser = $true
        }

        @{
            NodeName = 'DevReports'
            Role = @('REPORTS','All')
            CertificateFile = "C:\powershell-dsc\publickey\DSCPwdCert.cer"
            Thumbprint = "DDB37AEF86B21E298B3CD817E658F9CEFA1088C6"
            PSDscAllowDomainUser = $true
        }

        @{
            NodeName = 'ProdReports16'
            Role = @('REPORTS','All')
            CertificateFile = "C:\powershell-dsc\publickey\DSCPwdCert.cer"
            Thumbprint = "DDB37AEF86B21E298B3CD817E658F9CEFA1088C6"
            PSDscAllowDomainUser = $true
        }

        @{
            NodeName = 'DevRDS'
            Role = @('RDS','All')
            CertificateFile = "C:\powershell-dsc\publickey\DSCPwdCert.cer"
            Thumbprint = "DDB37AEF86B21E298B3CD817E658F9CEFA1088C6"
            PSDscAllowDomainUser = $true
        }
    )
}

### Credentials get set from prod-adCreds.csv file to add servers to domain as Domain Controllers
### Once the creds are set, they are passed on through the DSC modules

$Data = Import-csv -Path c:\powershell-dsc\prod-adCreds.csv
$realdomain = $Data.DomainName
$dsrmpass = $Data.NewSafemodeAdmin
$domcred = $Data.dompass
$dsrmcred = ConvertTo-SecureString -String $dsrmpass -Force -AsPlainText
$Cred2 = ConvertTo-SecureString -String $domcred -Force -AsPlainText
$Admincreds = New-Object System.Management.Automation.PSCredential ("$(($realdomain -split '\.')[0])\$(($Data.Domainadmin))", $Cred2)
$SafemodeAdmin = New-Object System.Management.Automation.PSCredential ('No UserName',$dsrmcred)


#### This block of code is where all the configurations get set by roles
#### The roles blocks have specific configuration needed for a roles
#### Some of these blocks are specific to a server, in order to set the DNS server addresses
configuration BasicTest {

    param 
    ( 
        [Parameter(Mandatory)]
        [String]$DomainName,

        [Parameter(Mandatory)]
        [PSCredential]$Admincreds,

        [Parameter(Mandatory)]
        [PSCredential]$SafemodeAdmincreds,

        [Int]$RetryCount=10,
        [Int]$RetryIntervalSec=30
    )

    if (!(Get-Module -ListAvailable -Name xActiveDirectory)) {Install-Module -Name xActiveDirectory -Force}
    Import-DscResource -ModuleName xActiveDirectory
    if (!(Get-Module -ListAvailable -Name xDHCPServer)) {Install-Module -Name xDHCPServer -Force}
    Import-DscResource -ModuleName xDHCPServer
    if (!(Get-Module -ListAvailable -Name xDnsServer)) {Install-Module -Name xDnsServer -Force}
	Import-DscResource -ModuleName xDnsServer
    if (!(Get-Module -ListAvailable -Name xPendingReboot)) {Install-Module -Name xPendingReboot -Force}
    Import-DscResource -ModuleName xPendingReboot
    Import-DscResource -ModuleName PSDesiredStateConfiguration
    if (!(Get-Module -ListAvailable -Name xNetworking)) {Install-Module -Name xNetworking -Force}
    Import-DSCResource -ModuleName xNetworking
    if (!(Get-Module -ListAvailable -Name xSmbShare)) {Install-Module -Name xSmbShare -Force}
    Import-DSCResource -ModuleName xSmbShare
    if (!(Get-Module -ListAvailable -Name cNtfsAccessControl)) {Install-Module -Name cNtfsAccessControl -Force}
	Import-DSCResource -ModuleName cNtfsAccessControl

    node $AllNodes.NodeName {
        switch($Node.Role) {
            #All servers get this configuration
            'All' {
                LocalConfigurationManager {
                    CertificateID = $Node.Thumbprint
                }

                Registry SSL2ServerDisabled {
					Ensure = 'Present'
					Key = 'HKLM:\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Protocols\SSL 2.0\Server'
					ValueName = 'Enabled'
					ValueData = 0
					ValueType = 'Dword'
				}
				
				Registry SSL3ServerDisabled {
					Ensure = 'Present'
					Key = 'HKLM:\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Protocols\SSL 3.0\Server'
					ValueName = 'Enabled'
					ValueData = 0
					ValueType = 'Dword'
				}
            }

            #Single Server Roles
			'DC10-15' {
				##This module tells the LCM to use a certificate for encryption
				xDnsServerAddress DnsServerAddress {
					Address        = '192.168.10.16','127.0.0.1'
					InterfaceAlias = 'Ethernet'
					AddressFamily  = 'IPv4'
					Validate       = $true
				}
			}
			'DC144-15' {
				xDnsServerAddress DnsServerAddress {
					Address        = '192.168.144.16','127.0.0.1'
					InterfaceAlias = 'Ethernet'
					AddressFamily  = 'IPv4'
					Validate       = $true
				}
			}
			'DC144-16' {
				xDnsServerAddress DnsServerAddress {
					Address        = '192.168.144.15','127.0.0.1'
					InterfaceAlias = 'Ethernet'
					AddressFamily  = 'IPv4'
					Validate       = $true
				}
			}
			'DC44-15' {
				xDnsServerAddress DnsServerAddress {
					Address        = '192.168.44.16','127.0.0.1'
					InterfaceAlias = 'Ethernet'
					AddressFamily  = 'IPv4'
					Validate       = $true
				}
			}
			'DC44-16' {
				xDnsServerAddress DnsServerAddress {
					Address        = '192.168.44.15','127.0.0.1'
					InterfaceAlias = 'Ethernet'
					AddressFamily  = 'IPv4'
					Validate       = $true
				}
			}
			'DC45-15' {
				xDnsServerAddress DnsServerAddress {
					Address        = '192.168.45.16','127.0.0.1'
					InterfaceAlias = 'Ethernet'
					AddressFamily  = 'IPv4'
					Validate       = $true
				}
			}
			'DC45-16' {
				xDnsServerAddress DnsServerAddress {
					Address        = '192.168.45.15','127.0.0.1'
					InterfaceAlias = 'Ethernet'
					AddressFamily  = 'IPv4'
					Validate       = $true
				}
			}
            
            #DHCP Servers
			'DHCP' {         
                @('DHCP', 'RSAT-DHCP','RSATDHCPTools').ForEach({

                    WindowsFeature $_
                        {
                            Ensure = "Present"
                            Name = $_
                        }
						})
                
				xDhcpServerAuthorization DHCPSrvrActivation {
					Ensure = 'Present'
					DependsOn = "[WindowsFeature]RSATDHCPTools"
				}
            }
			
			#Brand New Domain Controllers
			'NEWADDS' {
                @('AD-Domain-Services', 'RSAT-AD-Tools', 'RSAT-DNS-Server', 'RSAT-AD-PowerShell').ForEach({

                    WindowsFeature $_
                        {
                            Ensure = "Present"
                            Name = $_
                        }
                    })

                xWaitForADDomain DscForestWait { 
                    DomainName = $realdomain
                    DomainUserCredential= $Admincreds
                    RetryCount = $RetryCount
                    RetryIntervalSec = $RetryIntervalSec
                    DependsOn = "[WindowsFeature]ADDSInstall"
                }

                xADDomainController ReplicaDC { 
                    DomainName = $realdomain 
                    DomainAdministratorCredential = $Admincreds 
                    SafemodeAdministratorPassword = $SafemodeAdmin
                    DatabasePath = "C:\Windows\NTDS"
                    LogPath = "C:\Windows\NTDS"
                    SysvolPath = "C:\Windows\SYSVOL"
                    DependsOn = "[xWaitForADDomain]DscForestWait"
                }

                xPendingReboot Reboot1 { 
                    Name = "RebootServer"
                    DependsOn = "[xADDomainController]ReplicaDC"
                }
				
				xNetAdapterBinding EnableIPv6 {
					InterfaceAlias = 'Ethernet'
					ComponentId    = 'ms_tcpip6'
					State          = 'Enabled'
				}
				
				xDnsServerSetting DnsServerProperties {
					Name		= "DnsServerSettings"
					Forwarders 	= '208.67.222.222','208.67.220.220','8.8.8.8','8.8.4.4'
				}
            }
			
			#Member Servers becoming Domain Controllers
            'ADDS' {
                
                @('AD-Domain-Services', 'RSAT-AD-Tools', 'RSAT-DNS-Server', 'RSAT-AD-PowerShell').ForEach({

                    WindowsFeature $_
                        {
                            Ensure = "Present"
                            Name = $_
                        }
                    })

                xADDomainController ReplicaDC { 
                    DomainName = $realdomain 
                    DomainAdministratorCredential = $Admincreds 
                    SafemodeAdministratorPassword = $SafemodeAdmin
                    DatabasePath = "C:\Windows\NTDS"
                    LogPath = "C:\Windows\NTDS"
                    SysvolPath = "C:\Windows\SYSVOL"
                }
				
				xNetAdapterBinding EnableIPv6 {
					InterfaceAlias = 'Ethernet'
					ComponentId    = 'ms_tcpip6'
					State          = 'Enabled'
				}
				
				xDnsServerSetting DnsServerProperties {
					Name		= "DnsServerSettings"
					Forwarders 	= '208.67.222.222','208.67.220.220','8.8.8.8','8.8.4.4'
				}

            }
			
			#DFS Servers
			'DFSS' {
                @('FileAndStorage-Services', 'File-Services', 'FS-FileServer', 'FS-DFS-Namespace', 'FS-DFS-Replication', 'FS-Resource-Manager', 'Storage-Services', 'NET-Framework-45-Features', 'NET-Framework-45-Core', 'NET-WCF-Services45', 'NET-WCF-TCP-PortSharing45', 'RDC', 'RSAT', 'RSAT-Role-Tools', 'RSAT-File-Services', 'RSAT-DFS-Mgmt-Con', 'RSAT-FSRM-Mgmt', 'FS-SMB1', 'PowerShellRoot', 'PowerShell', 'PowerShell-ISE', 'WoW64-Support').ForEach({

                    WindowsFeature $_
                        {
                            Ensure = "Present"
                            Name = $_
                        }
                    })
            }
			
			#Backup Servers
			'BACKUP' {
                @('FileAndStorage-Services', 'File-Services', 'FS-FileServer', 'Storage-Services', 'NET-Framework-45-Features', 'NET-Framework-45-Core', 'NET-WCF-Services45', 'NET-WCF-TCP-PortSharing45', 'RDC', 'RSAT', 'RSAT-Role-Tools', 'RSAT-File-Services', 'FS-SMB1', 'PowerShellRoot', 'PowerShell', 'PowerShell-ISE', 'WoW64-Support').ForEach({

                    WindowsFeature $_
                        {
                            Ensure = "Present"
                            Name = $_
                        }
                    })
            }
			
			#Web Servers
			'WEB' {
                @('FileAndStorage-Services', 'File-Services', 'FS-FileServer', 'Storage-Services', 'Web-Server', 'Web-WebServer', 'Web-Common-Http', 'Web-Default-Doc', 'Web-Dir-Browsing', 'Web-Http-Errors', 'Web-Static-Content', 'Web-Http-Redirect', 'Web-DAV-Publishing', 'Web-Health', 'Web-Http-Logging', 'Web-Custom-Logging', 'Web-Log-Libraries', 'Web-ODBC-Logging', 'Web-Request-Monitor', 'Web-Http-Tracing', 'Web-Performance', 'Web-Stat-Compression', 'Web-Dyn-Compression', 'Web-Security', 'Web-Filtering', 'Web-Basic-Auth', 'Web-CertProvider', 'Web-Client-Auth', 'Web-Digest-Auth', 'Web-Cert-Auth', 'Web-IP-Security', 'Web-Url-Auth', 'Web-Windows-Auth', 'Web-App-Dev', 'Web-Net-Ext', 'Web-Net-Ext45', 'Web-AppInit', 'Web-ASP', 'Web-Asp-Net', 'Web-Asp-Net45', 'Web-CGI', 'Web-ISAPI-Ext', 'Web-ISAPI-Filter', 'Web-Includes', 'Web-WebSockets', 'Web-Mgmt-Tools', 'Web-Mgmt-Console', 'Web-Mgmt-Compat', 'Web-Metabase', 'Web-Lgcy-Mgmt-Console', 'Web-Lgcy-Scripting', 'Web-WMI', 'Web-Scripting-Tools', 'Web-Mgmt-Service', 'NET-Framework-Features', 'NET-Framework-Core', 'NET-HTTP-Activation', 'NET-Non-HTTP-Activ', 'NET-Framework-45-Features', 'NET-Framework-45-Core', 'NET-Framework-45-ASPNET', 'NET-WCF-Services45', 'NET-WCF-TCP-PortSharing45', 'RDC', 'RSAT', 'RSAT-Role-Tools', 'RSAT-File-Services', 'FS-SMB1', 'PowerShellRoot', 'PowerShell', 'PowerShell-V2', 'PowerShell-ISE', 'WAS', 'WAS-Process-Model', 'WAS-NET-Environment', 'WAS-Config-APIs', 'WoW64-Support').ForEach({

                    WindowsFeature $_
                        {
                            Ensure = "Present"
                            Name = $_
                        }
                    })
            }

            #Polaris Application Servers
			'APP' {
                @('FileAndStorage-Services', 'File-Services', 'FS-FileServer', 'Storage-Services', 'NET-Framework-Features', 'NET-Framework-Core', 'NET-Framework-45-Features', 'NET-Framework-45-Core', 'NET-WCF-Services45', 'NET-WCF-TCP-PortSharing45', 'MSMQ', 'MSMQ-Services', 'MSMQ-Server', 'RDC', 'FS-SMB1', 'PowerShellRoot', 'PowerShell', 'PowerShell-V2', 'PowerShell-ISE', 'WoW64-Support').ForEach({

                    WindowsFeature $_
                        {
                            Ensure = "Present"
                            Name = $_
                        }
                    })

                    @('PolarisSupport','PolarisBackups').ForEach({

                    File $_ {
                        Type = 'Directory'
                        DestinationPath = "C:\$_"
                        Ensure = "Present"
                }
            })

            }
            
            #Polaris Catalog Servers
            'PAC' {
                @('FileAndStorage-Services', 'File-Services', 'FS-FileServer', 'Storage-Services', 'Web-Server', 'Web-WebServer', 'Web-Common-Http', 'Web-Default-Doc', 'Web-Dir-Browsing', 'Web-Http-Errors', 'Web-Static-Content', 'Web-Health', 'Web-Http-Logging', 'Web-ODBC-Logging', 'Web-Performance', 'Web-Stat-Compression', 'Web-Security', 'Web-Filtering', 'Web-Basic-Auth', 'Web-Digest-Auth', 'Web-Windows-Auth', 'Web-App-Dev', 'Web-Net-Ext', 'Web-Net-Ext45', 'Web-ASP', 'Web-Asp-Net', 'Web-Asp-Net45', 'Web-ISAPI-Ext', 'Web-ISAPI-Filter', 'Web-Mgmt-Tools', 'Web-Mgmt-Console', 'Web-Scripting-Tools', 'Web-Mgmt-Service', 'NET-Framework-Features', 'NET-Framework-Core', 'NET-Framework-45-Features', 'NET-Framework-45-Core', 'NET-Framework-45-ASPNET', 'NET-WCF-Services45', 'NET-WCF-HTTP-Activation45', 'NET-WCF-TCP-PortSharing45', 'MSMQ', 'MSMQ-Services', 'MSMQ-Server', 'RDC', 'FS-SMB1', 'PowerShellRoot', 'PowerShell', 'PowerShell-V2', 'PowerShell-ISE', 'WAS', 'WAS-Process-Model', 'WAS-Config-APIs', 'WoW64-Support').ForEach({

                    WindowsFeature $_
                        {
                            Ensure = "Present"
                            Name = $_
                        }
                    })


                    cNtfsPermissionEntry bexley
                        {
                            Ensure = "Present"
                            Path = "C:\Program Files\Polaris\6.1\PowerPAC\custom\themes\bexley"
                            Principal = "jbumbico@bexleylibrary.org"
                            AccessControlInformation = @(
                                cNtfsAccessControlInformation
                                {
                                    AccessControlType = 'Allow'
                                    FileSystemRights = 'FullControl'
                                    Inheritance = 'ThisFolderSubfoldersAndFiles'
                                    NoPropagateInherit = $false
                                }
                            )
                        }

                        cNtfsPermissionEntry wl
                        {
                            Ensure = "Present"
                            Path = "C:\Program Files\Polaris\6.1\PowerPAC\custom\themes\wl"
                            Principal = "kreuter@worthingtonlibraries.org"
                            AccessControlInformation = @(
                                cNtfsAccessControlInformation
                                {
                                    AccessControlType = 'Allow'
                                    FileSystemRights = 'FullControl'
                                    Inheritance = 'ThisFolderSubfoldersAndFiles'
                                    NoPropagateInherit = $false
                                }
                            )
                        }


                @('bexley','wl').ForEach({
                    xSMBShare $_
                        {        

                            Ensure = "present"
                            Name   = "$_"
                            Path = "C:\Program Files\Polaris\6.1\PowerPAC\custom\themes\$_"
                            FullAccess = "Everyone"
                            Description = "PAC Theme Files for $_"         
                        }
                    })

            }

            #Polaris SimplyReports Servers
            'SR' {
                @('FileAndStorage-Services', 'File-Services', 'FS-FileServer', 'Storage-Services', 'Web-Server', 'Web-WebServer', 'Web-Common-Http', 'Web-Default-Doc', 'Web-Dir-Browsing', 'Web-Http-Errors', 'Web-Static-Content', 'Web-Http-Redirect', 'Web-Health', 'Web-Http-Logging', 'Web-ODBC-Logging', 'Web-Performance', 'Web-Stat-Compression', 'Web-Security', 'Web-Filtering', 'Web-Basic-Auth', 'Web-Digest-Auth', 'Web-Windows-Auth', 'Web-App-Dev', 'Web-Net-Ext', 'Web-Net-Ext45', 'Web-ASP', 'Web-Asp-Net', 'Web-Asp-Net45', 'Web-ISAPI-Ext', 'Web-ISAPI-Filter', 'Web-Mgmt-Tools', 'Web-Mgmt-Console', 'Web-Mgmt-Compat', 'Web-Metabase', 'Web-Scripting-Tools', 'Web-Mgmt-Service', 'NET-Framework-Features', 'NET-Framework-Core', 'NET-HTTP-Activation', 'NET-Framework-45-Features', 'NET-Framework-45-Core', 'NET-Framework-45-ASPNET', 'NET-WCF-Services45', 'NET-WCF-HTTP-Activation45', 'NET-WCF-TCP-PortSharing45', 'MSMQ', 'MSMQ-Services', 'MSMQ-Server', 'RDC', 'FS-SMB1', 'PowerShellRoot', 'PowerShell', 'PowerShell-V2', 'PowerShell-ISE', 'WAS', 'WAS-Process-Model', 'WAS-NET-Environment', 'WAS-Config-APIs', 'WoW64-Support').ForEach({

                    WindowsFeature $_
                        {
                            Ensure = "Present"
                            Name = $_
                        }     
                    })     
            
            }

            #SQL Reports Servers
            'REPORTS' {
                @('FileAndStorage-Services', 'File-Services', 'FS-FileServer', 'Storage-Services', 'NET-Framework-Features', 'NET-Framework-Core', 'NET-Framework-45-Features', 'NET-Framework-45-Core', 'NET-Framework-45-ASPNET', 'NET-WCF-Services45', 'NET-WCF-TCP-PortSharing45', 'MSMQ', 'MSMQ-Services', 'MSMQ-Server', 'RDC', 'FS-SMB1', 'Telnet-Client', 'PowerShellRoot', 'PowerShell', 'PowerShell-V2', 'PowerShell-ISE', 'WAS', 'WAS-Process-Model', 'WAS-Config-APIs', 'WoW64-Support', 'XPS-Viewer').ForEach({

                    WindowsFeature $_
                        {
                            Ensure = "Present"
                            Name = $_
                        }
                    })

            }

            #Remote Desktop Services Servers
            'RDS' {
            @('FileAndStorage-Services', 'File-Services', 'FS-FileServer', 'Storage-Services', 'Remote-Desktop-Services', 'RDS-RD-Server', 'NET-Framework-Features', 'NET-Framework-Core', 'NET-Framework-45-Features', 'NET-Framework-45-Core', 'NET-WCF-Ser
            vices45', 'NET-WCF-TCP-PortSharing45', 'Server-Media-Foundation', 'RDC', 'RSAT', 'RSAT-Role-Tools', 'RSAT-RDS-Tools', 'RSAT-RDS-Licensing-Diagnosis-UI', 'FS-SMB1', 'User-Interfaces-Infra', 'Server-Gui-Mgmt-Infra', 'Server-Gui-Shell', 'PowerShellRoot', 'PowerShell', 'PowerShell-V2', 'PowerShell-ISE', 'WoW64-Support').ForEach({
            
                    WindowsFeature $_
                         {
                            Ensure = "Present"
                            Name = $_
                         }
 
                    })
            }

            #Web Servers needing HTTPS redirect
            'HTTPSredirect' {
                @('FileAndStorage-Services', 'File-Services', 'FS-FileServer', 'Storage-Services', 'Remote-Desktop-Services', 'RDS-RD-Server', 'NET-Framework-Features', 'NET-Framework-Core', 'NET-Framework-45-Features', 'NET-Framework-45-Core', 'NET-WCF-Ser
                vices45', 'NET-WCF-TCP-PortSharing45', 'Server-Media-Foundation', 'RDC', 'RSAT', 'RSAT-Role-Tools', 'RSAT-RDS-Tools', 'RSAT-RDS-Licensing-Diagnosis-UI', 'FS-SMB1', 'User-Interfaces-Infra', 'Server-Gui-Mgmt-Infra', 'Server-Gui-Shell', 'PowerShellRoot', 'PowerShell', 'PowerShell-V2', 'PowerShell-ISE', 'WoW64-Support').ForEach({
                
                        WindowsFeature $_
                             {
                                Ensure = "Present"
                                Name = $_
                             }
     
                        })
                }
                
                #Install URL Rewrite module for IIS
                DependsOn = "[WindowsFeaturesWebServer]windowsFeatures"
                Ensure = "Present"
                Name = "IIS URL Rewrite Module 2"
                Path = "http://download.microsoft.com/download/6/7/D/67D80164-7DD0-48AF-86E3-DE7A182D6815/rewrite_2.0_rtw_x64.msi"
                Arguments = "/quiet"
                ProductId = "EB675D0A-2C95-405B-BEE8-B42A65D23E11"

            Script HTTPSRedirect
            {   
                #Must return a hashtable with at least one key named 'Result' of type String
                GetScript = {
                  Return @{
                    Result = [string]$(Get-WebConfigurationProperty -PSPath 'IIS:\Sites\Default Web Site' -Filter "system.webServer/rewrite/rules/rule[@name='HTTP to HTTPS redirect']/conditions" -name collection[0].pattern.value)
                  }  
                }
                
                #Tests the configuration, must return true or false
                TestScript = {
                if ((Get-WebConfigurationProperty -PSPath 'IIS:\Sites\Default Web Site' -Filter "system.webServer/rewrite/rules/rule[@name='HTTP to HTTPS redirect']/conditions" -name collection[0].pattern.value) -like "*off*"){
                    return $true
                } else {return $false}

                #Sets the configuration, returns nothing
                SetScript = {
                    $site = "IIS:\Sites\Default Web Site"
                    $filterRoot = "system.webServer/rewrite/rules/rule[@name='HTTP to HTTPS redirect$_']"
                    Clear-WebConfiguration -filter $filterRoot
                    Add-WebConfigurationProperty -pspath $site -filter "system.webServer/rewrite/rules" -name "." -value @{name='HTTP to HTTPS redirect' + $_ ;patternSyntax='Regular Expressions';stopProcessing='False'}
                    Set-WebConfigurationProperty -pspath $site -filter "$filterRoot/match" -name "url" -value "(.*)"
                    Set-WebConfigurationProperty -pspath $site -filter "$filterRoot/conditions" -name "logicalGrouping" -value "MatchAll"
                    Set-WebConfigurationProperty -pspath $site -filter "$filterRoot/conditions" -name "." -value @{
                        input = '{HTTPS}'
                        matchType ='0'
                        pattern ='^OFF$'
                        ignoreCase ='True'
                    }
                    Set-WebConfigurationProperty -pspath $site -filter "$filterRoot/action" -name "type" -value "Redirect"
                    Set-WebConfigurationProperty -pspath $site -filter "$filterRoot/action" -name "url" -value 'https://{HTTP_HOST}{REQUEST_URI}'

                }

                }

            }                


    }
}

}

BasicTest -DomainName $realdomain -Admincreds $Admincreds -SafemodeAdmincreds $SafemodeAdmin -OutputPath "C:\powershell-dsc\HTTPS\adds" -ConfigurationData $configData

#The MOF files need moved to the DSC pull server
Copy-Item -Path C:\powershell-dsc\HTTPS\adds\*.* -Destination "\\dc45-dsc.clcdpc.org\c$\Program Files\WindowsPowerShell\DscService\Configuration\" -Force

#These MOF files now need checksums
$filestocheck = Get-ChildItem -Path "\\dc45-dsc.clcdpc.org\c$\Program Files\WindowsPowerShell\DscService\Configuration\" | Where-Object { $_.Name -like "*.mof" -and $_.Name -notlike "*.meta.mof" } | Select-Object -ExpandProperty Name
foreach ($pcname in $filestocheck) {
    $dest =  "\\dc45-dsc.clcdpc.org\c$\Program Files\WindowsPowerShell\DscService\Configuration\$pcname"
    New-DscChecksum $dest -Force
}

#Uncomment and fill in below to force a pull of DSC
Update-DscConfiguration -ComputerName devpac.clcdpc.org -Verbose -Wait