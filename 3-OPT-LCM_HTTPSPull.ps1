<# Author: Jason Helmick and Jeffrey Snover
#
# Modified by: Paul Robson
#
# Description: This script/module for DSC configures the LCM for the DSC nodes
#
#> 

[DSCLocalConfigurationManager()]
Configuration LCM_HTTPPULL 
{
    param
        (
            [Parameter(Mandatory=$true)]
            [string[]]$ComputerName

        )      	
    Node $ComputerName {  
	    Settings{
                RefreshFrequencyMins = 30;
                RefreshMode = 'Pull';
                ConfigurationMode = 'ApplyAndAutoCorrect';
                AllowModuleOverwrite = $True;
                RebootNodeIfNeeded = $false;
                CertificateID = 'DDB37AEF86B21E298B3CD817E658F9CEFA1088C6';                
            }
        ConfigurationRepositoryWeb DSCHTTPS {
                ServerURL = 'https://dc45-dsc.clcdpc.org:8045/PSDSCPullServer.svc'
				RegistrationKey = '6fc09cdf-7f95-4a8b-b275-13417fe9301a'
				ConfigurationNames = @("$ComputerName")
            }
			
		ReportServerWeb DSCHTTPS {
                ServerURL = 'https://dc45-dsc.clcdpc.org:8045/PSDSCPullServer.svc'
            }
    }
}

# Computer list 
$ComputerName = $env:computername

# Create Guid for the computers
#$guid=[guid]::NewGuid()
#$regkey = [guid]::NewGuid()
#$guid = Get-DscLocalConfigurationManager | Select -ExpandProperty ConfigurationID

# Create the Computer.Meta.Mof in folder
LCM_HTTPPULL -ComputerName $ComputerName -OutputPath C:\powershell-dsc\HTTPS

# Send to computers LCM
Set-DSCLocalConfigurationManager -ComputerName $ComputerName -Path C:\powershell-dsc\HTTPS -Verbose -Force
