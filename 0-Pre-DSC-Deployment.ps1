<# Author: Jason Helmick and Jeffrey Snover
#
# Modified by: Paul Robson, Sam Lewis
#
# Modified last: 1/7/2019
#
# Description: This script needs to run from a Hyper-V host PS session. All the resources are under R:\vmtemplates
#				More detailed information can be found in comments throughout script.
#
#> 

Invoke-Command -ComputerName hyperv09 -ScriptBlock {


## Two cmdlets below query the default path for VM and VHD storage
$defaultVHDPath=(Get-ItemProperty -Path 'HKLM:\Software\Microsoft\Windows NT\CurrentVersion\Virtualization'  -Name 'DefaultVirtualHardDiskPath').DefaultVirtualHardDiskPath 
$defaultVMPath=(Get-ItemProperty -Path 'HKLM:\Software\Microsoft\Windows NT\CurrentVersion\Virtualization'  -Name 'DefaultExternalDataRoot').DefaultExternalDataRoot

## Cmdlet below set the directories for where VM resources
$templatePath = "R:\vmtemplates"

## You set the name of the new VM here
$newVMName = "ProdZ3916"

## Set where the new VM will be stored
$newVMPath = "$defaultVMPath\$newVMName"

## Set where the VHDX for the new VM will be stored
$newVHD = "$defaultVMPath\$newVMName\$newVMName.vhdx"

## Set the name of the virtual switch that will be used
$currentVMSwitch = "LanVirtualSwitch"

## Create directory of where the new VM will live
mkdir "$defaultVMPath\$newVMName"

## Copy the image of the Windows Server 2016 you want to location of new VM. Options are Core and FullGUI
## Options for files: WinSrvr2016-Core.vhdx  OR  win2016FullGUI.vhdx
#Copy-Item $templatePath\WinSrvr2016-Core.vhdx -Destination "$defaultVMPath\$newVMName\$newVMName.vhdx"
Copy-Item $templatePath\win2016FullGUI.vhdx -Destination "$defaultVMPath\$newVMName\$newVMName.vhdx"

## Set the RAM and # of CPUs to use for new VM
New-VM -Name $newVMName -MemoryStartupBytes 8192MB -VHDPath $newVHD -Path $defaultVMPath -Generation 2
Get-VM -Name $newVMName | Set-VMProcessor -Count 8

#==================================================================

## This XML template sets some basic information, then copies it into a file with the new VM name
## and then it copies it to the new VM folder
# Edit unattend.xml template

$AdminPassword='WWeellccoommee!!'
$Organization='Central Library Consortium'
$Owner='Central Library Consortium'
$TimeZone='Eastern Standard Time'

$UnattendTemplate="$templatePath\unattend-template.xml" 
$Unattendfile=New-Object XML 
$Unattendfile.Load($UnattendTemplate) 
$Unattendfile.unattend.settings.component[0].ComputerName=$newVMName
$Unattendfile.unattend.settings.component[0].RegisteredOrganization=$Organization 
$Unattendfile.unattend.settings.component[0].RegisteredOwner=$Owner 
$Unattendfile.unattend.settings.component[0].TimeZone=$TimeZone 
$Unattendfile.unattend.settings.Component[1].RegisteredOrganization=$Organization 
$Unattendfile.unattend.settings.Component[1].RegisteredOwner=$Owner 
$UnattendFile.unattend.settings.component[1].UserAccounts.AdministratorPassword.Value=$AdminPassword
$UnattendFile.unattend.settings.component[1].autologon.password.value=$AdminPassword 
 
$UnattendXML=$templatePath+"\"+$newVMName+".xml" 

 
$Unattendfile.save($UnattendXML)

Copy-Item $UnattendXML -Destination ("$defaultVMPath\$newVMName\$newVMName"+".xml")

#=======================================================================

# 
# In this section, once XML template has been created, it will inject the settings into the VHDX of the VM
#

## Mounts the VHDX of the new VM in the Hyper-V host so files can be copied into it
Mount-DiskImage -ImagePath $newVHD -StorageType VHDX

## Stores drive letter used in Hyper-V host for VHDX
$drvLetter = (Get-DiskImage -ImagePath $newVHD | Get-Disk | Get-Partition).DriveLetter.Get(3)+":"

## Sets destination of XML inside the VHDX
$Destination = $drvLetter+"\Windows\System32\Sysprep\unattend.xml"

## Sets and creates folder where PS scripts will go
$ScriptFolder = $drvLetter+"\ProgramData\Scripts\" 
New-Item $ScriptFolder -ItemType Directory

## Sets and creates where PowerShell DSC related scripts will go
$psDSCFolder = $drvLetter+"\powershell-dsc\HTTPS\"
New-Item $psDSCFolder -ItemType Directory

## Sets and creates where PowerShell DSC related keys will go
$dscCertFolder = $drvLetter+"\powershell-dsc\publickey\"
New-Item $dscCertFolder -ItemType Directory

## Copies CMD file that runs a couple of PS scripts at startup
Copy-Item "$templatePath\FirstStart.cmd" $ScriptFolder

## Copies CMD file that runs another PS script at second startup
Copy-Item "$templatePath\SecondStart.cmd" $ScriptFolder

## Copies CSV document that contains Admin creds to join VM to domain
Copy-Item "$templatePath\prod-adCreds.csv" $ScriptFolder

## Copies PS script setting the LCM client configurations
## IF server will NOT BE A DSC NODE, you can comment this cmdlet out
Copy-Item "$templatePath\3-OPT-LCM_HTTPSPull.ps1" $ScriptFolder

## Copies PS script containing settings (joining to domain, license, IP address) for new VM
Copy-Item "$templatePath\1-NewNode-Pre-DSC-Deployment.ps1" $ScriptFolder

## Copies script containing specific firewall rules for servers
Copy-Item "$templatePath\4-VMDeployment-FW-DomainRules.ps1" $ScriptFolder

## Copies DSC resource modules from central place to new VM
Copy-Item "$templatePath\dscresources\*" -Destination "$drvLetter\Program Files\WindowsPowerShell\Modules" -Recurse -Force

## Copies certificates needed for DSC
Copy-Item "$templatePath\publicKey\DSCPwdCert.cer" $dscCertFolder
Copy-Item "$templatePath\publicKey\DSCCert.pfx" $dscCertFolder
Copy-Item "$templatePath\publicKey\DSCPwdCertPFX.pfx" $dscCertFolder

## Copies XML unattended file to destination in new VM
Copy-Item $UnattendXML $Destination

## Opens registry path, and sets FirstStart.cmd to run on first startup
$RemoteReg = $drvLetter+"\Windows\System32\config\Software"
REG LOAD 'HKLM\REMOTEPC' $RemoteReg
NEW-ITEMPROPERTY "HKLM:REMOTEPC\Microsoft\Windows\CurrentVersion\RunOnce\" -Name "PoshStart" -Value "C:\ProgramData\Scripts\FirstStart.cmd"
REG UNLOAD 'HKLM\REMOTEPC'

## Dismounts new VM's VHDX
dismount-diskimage -ImagePath $newVHD

#==============================================================================

## Connect new VM to VM Switch
Connect-VMNetworkAdapter -VMName $newVMName -SwitchName $currentVMSwitch

## Set VM network adapter for a specific VM to use Access mode to a certain VLAN
Set-VMNetworkAdapterVlan -VMName $newVMName -Access -VlanId 44

## Enable Time Sync service on a VM. This needs to be enabled on all non-DCs
Enable-VMIntegrationService -VMName $newVMName -name 'time synchronization'

## Starts the new VM
Start-VM $newVMName
}