#Creates an array of the features to be added, and then formats them the way DSC looks for features (see 5-DSCnodes-basicConfig.ps1)
#Note some features of 2012 R2 are no longer available in 2016, such as Application-Server (and subfeatures), and Server-Gui features [2016 no longer allows dynamic switch between Core and GUI versions]

$oldserver = "devrds01"

$features = (get-windowsfeature -cn $oldserver | Where-Object installstate -like "Installed" | Select-Object name)

@"
                 @('$($features.Name -Join "', '")').ForEach({

                    WindowsFeature `$_
                        {
                            Ensure = "Present"
                            Name = `$_
                        }

"@