configuration HTTPSPullServer
{

	## TESTING - From MS https://docs.microsoft.com/en-us/powershell/dsc/secureserver
	<#param (
        [Parameter(Mandatory=$true)]
        [ValidateNotNullorEmpty()]
        [System.String] $ServerName,
        [System.String] $DomainName,
        [System.String] $CARootName,
        [System.String] $CAServerFQDN,
        [System.String] $CertSubject
        #[System.String] $SMBShare,
        #[Parameter(Mandatory=$true)]
        #[ValidateNotNullorEmpty()]
        #[PsCredential] $Credential
    )#>
	
    # Modules must exist on target pull server
    Import-DSCResource -ModuleName xPSDesiredStateConfiguration
	Import-DSCResource -ModuleName PSDesiredStateConfiguration
	Import-DSCResource -ModuleName xWebAdministration
	Import-DSCResource -ModuleName xCertificate
	Import-DSCResource -ModuleName xAdcsDeployment

    Node dc45-dsc.clcdpc.org
    {
		LocalConfigurationManager
        {
            ConfigurationMode = 'ApplyAndAutoCorrect'
            RebootNodeifNeeded = $false
        }
		
        WindowsFeature DSCServiceFeature {
            Ensure = "Present"
            Name   = "DSC-Service"
        }

        WindowsFeature IIS {
        
            Ensure = "Present"
            Name = "Web-Server"
        }
		
		WindowsFeature IISConsole {
        
            Ensure = "Present"
            Name = "Web-Mgmt-Console"
        }
		
		WindowsFeature IISScriptingTools	{
        
            Ensure = "Present"
            Name = "Web-Scripting-Tools"
        }
		
		WindowsFeature IISSixCompatibility	{
        
            Ensure = "Present"
            Name = "Web-Mgmt-Compat"
        }

        # !!!!! # GUI Remote Management of IIS requires the following: - people always forget this until too late

        WindowsFeature Management {
			
            Ensure = "Present"
            Name = "Web-Mgmt-Service"
        }

        WindowsFeature CertificationAuthority {
            
            Name = 'ADCS-Cert-Authority'
            Ensure = 'Present'
        }
		
		## TESTING - From MS https://docs.microsoft.com/en-us/powershell/dsc/secureserver
		# The next series of settings disable SSL and enable TLS, for environments where that is required by policy.
        Registry TLS1_2ServerEnabled {
            Ensure = 'Present'
            Key = 'HKLM:\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Protocols\TLS 1.2\Server'
            ValueName = 'Enabled'
            ValueData = 1
            ValueType = 'Dword'
        }
		
		## TESTING - From MS https://docs.microsoft.com/en-us/powershell/dsc/secureserver
        Registry TLS1_2ServerDisabledByDefault {
            Ensure = 'Present'
            Key = 'HKLM:\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Protocols\TLS 1.2\Server'
            ValueName = 'DisabledByDefault'
            ValueData = 0
            ValueType = 'Dword'
        }
		
		## TESTING - From MS https://docs.microsoft.com/en-us/powershell/dsc/secureserver
        Registry TLS1_2ClientEnabled {
            Ensure = 'Present'
            Key = 'HKLM:\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Protocols\TLS 1.2\Client'
            ValueName = 'Enabled'
            ValueData = 1
            ValueType = 'Dword'
        }
		
		## TESTING - From MS https://docs.microsoft.com/en-us/powershell/dsc/secureserver
        Registry TLS1_2ClientDisabledByDefault {
            Ensure = 'Present'
            Key = 'HKLM:\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Protocols\TLS 1.2\Client'
            ValueName = 'DisabledByDefault'
            ValueData = 0
            ValueType = 'Dword'
        }
		
		## TESTING - From MS https://docs.microsoft.com/en-us/powershell/dsc/secureserver
        Registry SSL2ServerDisabled {
            Ensure = 'Present'
            Key = 'HKLM:\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Protocols\SSL 2.0\Server'
            ValueName = 'Enabled'
            ValueData = 0
            ValueType = 'Dword'
        }
		
		## TESTING - From MS https://docs.microsoft.com/en-us/powershell/dsc/secureserver
        Registry SSL3ServerDisabled {
            Ensure = 'Present'
            Key = 'HKLM:\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Protocols\SSL 3.0\Server'
            ValueName = 'Enabled'
            ValueData = 0
            ValueType = 'Dword'
        }

        Registry RemoteManagement { # Can set other custom settings inside this reg key

            Key = 'HKLM:\SOFTWARE\Microsoft\WebManagement\Server'
            ValueName = 'EnableRemoteManagement'
            ValueType = 'Dword'
            ValueData = '1'
            DependsOn = @('[WindowsFeature]IIS','[WindowsFeature]Management')
        }

        Service StartWMSVC {

            Name = 'WMSVC'
            StartupType = 'Automatic'
            State = 'Running'
            DependsOn = '[Registry]RemoteManagement'
        }
		
		## DO NOT NEED for DSC Pull server to work
		## TESTING - From MS https://docs.microsoft.com/en-us/powershell/dsc/secureserver
		<#xCertReq SSLCert {
            CARootName = $Node.CARootName
            CAServerFQDN = $Node.CAServerFQDN
            Subject = $Node.CertSubject
            AutoRenew = $Node.AutoRenew
            Credential = $Node.Credential
        }#>

        xDscWebService PSDSCPullServer {
            Ensure                  = "Present"
            EndpointName            = "PSDSCPullServer"
            Port                    = 8045
            PhysicalPath            = "$env:SystemDrive\inetpub\wwwroot\PSDSCPullServer"
            ## Insert Thumprint for DIGICERT Wildcard cert for clcdpc.org
            CertificateThumbPrint   = 'C269FACF2F05B3DC1F9BC05035E9AD958C9FA3D9'
            ModulePath              = "$env:PROGRAMFILES\WindowsPowerShell\DscService\Modules"
            ConfigurationPath       = "$env:PROGRAMFILES\WindowsPowerShell\DscService\Configuration"
            State                   = "Started"
			RegistrationKeyPath     = "$env:PROGRAMFILES\WindowsPowerShell\DscService"
            UseSecurityBestPractices = $true
            DependsOn               = ("[WindowsFeature]DSCServiceFeature","[WindowsFeature]IISConsole","[WindowsFeature]Management")
        }

        xDscWebService PSDSCComplianceServer {
            Ensure                  = "Present"
            EndpointName            = "PSDSCComplianceServer"
            Port                    = 9045
            PhysicalPath            = "$env:SystemDrive\inetpub\wwwroot\PSDSCComplianceServer"
            ## Insert Thumprint for DIGICERT Wildcard cert for clcdpc.org
            CertificateThumbPrint   = 'C269FACF2F05B3DC1F9BC05035E9AD958C9FA3D9'
            State                   = "Started"
            UseSecurityBestPractices = $true
            DependsOn               = ("[WindowsFeature]DSCServiceFeature","[xDSCWebService]PSDSCPullServer")
        }
		
		# DO NOT ENABLE this configs, Server 2016 DSC works on ESENT DB better than MDB
		## From MS https://docs.microsoft.com/en-us/powershell/dsc/secureserver
        		
		<#xWebConfigKeyValue CorrectDBProvider
        { 
            ConfigSection = 'AppSettings'
            Key = 'dbprovider'
            Value = 'System.Data.OleDb'
            WebsitePath = 'IIS:\sites\PSDSCPullServer'
            DependsOn = '[xDSCWebService]PSDSCPullServer'
        }

        xWebConfigKeyValue CorrectDBConnectionStr
        { 
            ConfigSection = 'AppSettings'
            Key = 'dbconnectionstr'            
            Value = if ([System.Environment]::OSVersion.Version -gt '6.3.0.0') #greater then Windows Server 2012 R2
            {
                'Provider=Microsoft.Jet.OLEDB.4.0;Data Source=C:\Program Files\WindowsPowerShell\DscService\Devices.mdb;' #does no longer work with Server 2016+
            }
            else
            {
                'Provider=Microsoft.ACE.OLEDB.12.0;Data Source=C:\Program Files\WindowsPowerShell\DscService\Devices.mdb;'
            }
            WebsitePath = 'IIS:\sites\PSDSCPullServer'
            DependsOn = '[xDSCWebService]PSDSCPullServer'
        }#>

		## TESTING - From MS https://docs.microsoft.com/en-us/powershell/dsc/secureserver
        # Stop the default website
        xWebsite StopDefaultSite {
            Ensure = 'Present'
            Name = 'Default Web Site'
            State = 'Stopped'
            PhysicalPath = 'C:\inetpub\wwwroot'
            DependsOn = '[WindowsFeature]DSCServiceFeature'
        }

        File RegistrationKeyFile {
            Ensure          = 'Present'
            Type            = 'File'
            DestinationPath = "$env:ProgramFiles\WindowsPowerShell\DscService\RegistrationKeys.txt"
            Contents        = '6fc09cdf-7f95-4a8b-b275-13417fe9301a'
        }
    }
}

# Generate MOF

HTTPSPullServer -OutputPath C:\powershell-dsc\HTTPS

