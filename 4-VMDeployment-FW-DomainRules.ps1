## Allow firewall rules after joined to domain

$ScriptsRoot = "C:\Programdata\Scripts"
@('FirstStart.cmd','newNode-PreDSC.ps1','prod-adCreds.csv').ForEach({   if (test-path $ScriptsRoot\$_) {Remove-Item $ScriptsRoot\$_ -Force} })

@('FPS-ICMP4-ERQ-IN','WINRM-HTTP-IN-TCP','FPS-SMB-In-TCP','FPS-SMB-Out-TCP').ForEach({Set-NetFirewallRule -Name $_ -Enable True -Action Allow -Profile Domain})

@('Remote Service Management (RPC)','Remote Service Management (RPC-EPMAP)','Remote Service Management (NP-In)','Remote Event Log Management (RPC)','Remote Event Log Management (RPC-EPMAP)','Remote Event Log Management (NP-In)','Remote Volume Management - Virtual Disk Service (RPC)','Remote Volume Management (RPC-EPMAP)','Remote Event Log Management (NP-In)','Remote Volume Management - Virtual Disk Service (RPC)','Remote Volume Management (RPC-EPMAP)','Remote Desktop - User Mode (UDP-In)','Remote Desktop - User Mode (TCP-In)','Remote Desktop - Shadow (TCP-In)').ForEach({
    Set-NetFirewallRule -DisplayName "$_" -Enabled True -Action Allow -Profile Domain
})
