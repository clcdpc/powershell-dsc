configuration CopyDSCResources {
    Node AllDSCNodes {

        File CopyDscResources {
                    Ensure = "Present"
                    Type = "Directory"
                    Recurse = $true
                    Matchsource = $true
                    Sourcepath = "\\testdc02\DscResources"
                    DestinationPath = "$env:ProgramFiles\WindowsPowerShell\Modules"
        }
    }
}
CopyDSCResources -OutputPath C:\powershell-dsc
