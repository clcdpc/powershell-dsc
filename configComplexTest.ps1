<# Author: Jason Helmick and Jeffrey Snover
#
# Modified by: Paul Robson
#
# Description: This script/module for DSC node should create VM by duplicating an existing VHDX.
#	Once VM is running, it should configure it as another DC.
# 	It also copies all necessary DSC resource modules from a share in DSC server to the DSC node
# 	
#
#> 

$configData = @{
    AllNodes = @(
        <#@{
            NodeName = 'TESTDC02'
            Role = @(
                'SNMP'
            )
        }#>

        @{
            NodeName = 'TESTWS2016'
            Role = @(
                'ADDS'
            )
            CertificateFile = "\\testdc02\PublicKey\DSCPwdCert.cer"
            Thumbprint = "40E6EED25BF7DF2EE7CE3BFDCCA4F8CB66880BB0"
            PSDscAllowDomainUser = $true
        }

        <#@{
            NodeName = 'TESTCERT'
            Role = @(
                'CERT'
            )
            CertificateFile = "\\testdc02\publicKey\DSCPwdCert.cer"
            Thumbprint = "B9B1F4B27BCFB958361E2F866A9D79BC95C34A5C"
            PSDscAllowDomainUser = $true
        }#>
    )

    <#  FileServerTypes = @{
        'StandardFileServer' = @{
            Shares = @(
                @{
                    Path = 'S:\SomeFolder'
                    ShareName = 'Public'
                    # Permissions, etc
                }
            )
        }
    } #>
}

$Data = Import-csv -Path c:\powershell-dsc\adCreds.csv
$realdomain = $Data.DomainName
$Password = $Data.NewSafemodeAdmin
$domcred = $Data.dompass
$Cred2 = ConvertTo-SecureString -String $domcred -Force -AsPlainText
$Admincreds = New-Object System.Management.Automation.PSCredential ("$(($realdomain -split '\.')[0])\$(($Data.Domainadmin))", $Cred2)
#$Cred = ConvertTo-SecureString -String $Password -Force -AsPlainText
$SafemodeAdmin = New-Object System.Management.Automation.PSCredential ('No UserName',$Cred2)

configuration BasicTest {

    param 
    ( 
        [Parameter(Mandatory)]
        [String]$DomainName,

        [Parameter(Mandatory)]
        [PSCredential]$Admincreds,

        [Parameter(Mandatory)]
        [PSCredential]$SafemodeAdmincreds,

        [Int]$RetryCount=20,
        [Int]$RetryIntervalSec=30
    )

    Import-DscResource -ModuleName xActiveDirectory
    Import-DscResource -ModuleName xPendingReboot
    Import-DscResource -ModuleName PSDesiredStateConfiguration

    node $AllNodes.NodeName {
        switch($Node.Role) {
            <#'CERT' {
                # This setting copies all DSC resources suggested by DSC folks
                File CopyDscResources {
                    Ensure = "Present"
                    Type = "Directory"
                    Recurse = $true
                    Matchsource = $true
                    Sourcepath = "\\testdc02\DscResources"
                    DestinationPath = "$env:ProgramFiles\WindowsPowerShell\Modules"
                }

                LocalConfigurationManager {
                    CertificateID = $Node.Thumbprint
                }

                WindowsFeature ADCertAuthority { 
                    Ensure = "Absent" 
                    Name = "ADCS-Cert-Authority"
                }

                WindowsFeature GroupPolicy {  
                    Ensure = "Present"  
                    Name = "GPMC"
                }
            }#>

            'ADDS' {
				# This setting copies all DSC resources suggested by DSC folks
                File CopyDscResources {
                    Ensure = "Present"
                    Type = "Directory"
                    Recurse = $true
                    Matchsource = $true
                    Sourcepath = "\\testdc02\DscResources"
                    DestinationPath = "$env:ProgramFiles\WindowsPowerShell\Modules"
                }

                LocalConfigurationManager {
                    CertificateID = $Node.Thumbprint
                }

                WindowsFeature ADDSInstall { 
                    Ensure = "Present" 
                    Name = "AD-Domain-Services"
					DependsOn = "[File]CopyDscResources"
                }

                WindowsFeature ADTools {  
                    Ensure = "Present"  
                    Name = "RSAT-AD-Tools"  
                    DependsOn = "[WindowsFeature]ADDSInstall"  
                }

                WindowsFeature DNSServerTools {  
                    Ensure = "Present"  
                    Name = "RSAT-DNS-Server"
                    DependsOn = "[WindowsFeature]ADTools"  
                }

                WindowsFeature RSATADPowerShell {
                    Ensure = "Present"
                    Name = "RSAT-AD-PowerShell"
                    DependsOn = "[WindowsFeature]DNSServerTools"
                }

                xWaitForADDomain DscForestWait { 
                    DomainName = $realdomain
                    DomainUserCredential= $Admincreds
                    RetryCount = $RetryCount
                    RetryIntervalSec = $RetryIntervalSec
                    DependsOn = "[WindowsFeature]ADDSInstall"
                }

                xADDomainController ReplicaDC { 
                    DomainName = $realdomain 
                    DomainAdministratorCredential = $Admincreds 
                    SafemodeAdministratorPassword = $SafemodeAdmin
                    DatabasePath = "C:\NTDS"
                    LogPath = "C:\NTDS"
                    SysvolPath = "C:\SYSVOL"
                    DependsOn = "[xWaitForADDomain]DscForestWait"
                }

                xPendingReboot Reboot1 { 
                    Name = "RebootServer"
                    DependsOn = "[xADDomainController]ReplicaDC"
                }
            }
        }
    }
}

BasicTest -OutputPath "C:\powershell-dsc\HTTPS\cert" -ConfigurationData $configData

#BasicTest -DomainName $realdomain -Admincreds $Admincreds -SafemodeAdmincreds $SafemodeAdmin -OutputPath "C:\powershell-dsc\HTTPS\cert" -ConfigurationData $configData

#Set-DSCLocalConfigurationManager -ComputerName testws2012 -Path C:\powershell-dsc\HTTP\adds –Verbose -Force

#Start-DscConfiguration -Path "C:\powershell-dsc\HTTP\adds" -ComputerName testws2012 -Verbose -Wait -Force